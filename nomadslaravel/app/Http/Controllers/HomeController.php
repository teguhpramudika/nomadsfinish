<?php

namespace App\Http\Controllers;
use App\Travelpackage;
use Illuminate\Http\Request;

class HomeController extends Controller
{


    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $items =Travelpackage::with(['galleries'])->get();
        return view('pages.home',[
            'items'=>$items
        ]);
    }
}
