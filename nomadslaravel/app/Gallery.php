<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;


class Gallery extends Model
{
    use SoftDeletes;

     protected $table = 'galleries';

    protected $fillable =[

        'travel_packages_id','image'

    ];

    protected $hidden =[

 
    ] ;

    public function travel_package() {

        return $this->belongsTo(Travelpackage::class,'travel_packages_id','id');
    }

}
