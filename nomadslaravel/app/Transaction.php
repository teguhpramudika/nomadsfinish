<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Transaction extends Model
{

    use SoftDeletes;
    protected $table = 'transactions';

    protected $fillable = [

        'travel_packages_id','users_id','additional_visa','transaction_total','transaction_status'
    ];

    protected $hidden = [

        //
    ];

    public function details() {

        return $this->hasmany(Transactiondetail::class,'transactions_id','id');
       }

       public function travel_package() {

        return $this->belongsto(Travelpackage::class,'travel_packages_id','id');
       }


       public function user() {

        return $this->belongsto(User::class,'users_id','id');
       }

}
